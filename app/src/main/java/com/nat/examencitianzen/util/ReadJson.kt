package com.nat.examencitianzen.util

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.annotation.RawRes
import androidx.core.app.ActivityCompat
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nat.examencitianzen.MyApp
import com.nat.examencitianzen.R
import com.nat.examencitianzen.data.model.Transaction
import java.util.*

object ReadJson {

    private  val gson: Gson = Gson()

    private inline fun <reified T> readRawJson(@RawRes rawResId: Int): T {
        MyApp.instance.resources.openRawResource(rawResId).bufferedReader().use {
            return gson.fromJson<T>(it, object : TypeToken<T>() {}.type)
        }
    }
    fun read():Transaction{
        return readRawJson(R.raw.transa)
    }

}

object CheckPermissions{

    fun requestPermissions(activity: Activity?, requestId: Int, vararg permissions: String?) {
        ActivityCompat.requestPermissions(activity!!, permissions, requestId)
    }

    fun checkPermission(context: Context?, vararg permissions: String?): Boolean {
        for (permission in permissions) {
            if (ActivityCompat.checkSelfPermission(
                    context!!,
                    permission!!
                ) != PackageManager.PERMISSION_GRANTED
            ) return false
        }
        return true
    }
    fun isPermissionGranted(grantPermissions: Array<out String>, grantResult: IntArray, vararg permissions: String?): Boolean {
        var isAllPermissionsGranted = true
        if (Arrays.equals(permissions, grantPermissions)) {
            for (i in grantPermissions.indices) {
                if (grantResult[i] != PackageManager.PERMISSION_GRANTED) {
                    isAllPermissionsGranted = false
                    break
                }
            }
        }
        return isAllPermissionsGranted
    }
}