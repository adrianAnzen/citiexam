package com.nat.examencitianzen.util

import android.content.Context
import android.content.SharedPreferences
import com.nat.examencitianzen.MyApp

object Constants {
     const val LOCATION_PERMISSION = 101
}

object SharedPreferencesManager{
     private const val APP_SETTING_FILE = "APP_FILE"

     private fun getSharedPreferences(): SharedPreferences {
          return MyApp.instance.applicationContext.getSharedPreferences(APP_SETTING_FILE, Context.MODE_PRIVATE)
     }

     fun setSomeStringValue(valueLabel: String?, dataValue: String?) {
          val editor = getSharedPreferences().edit()
          editor.putString(valueLabel, dataValue)
          editor.apply()
     }


     fun getSomeStringValue(valueLabel: String?): String? {
          return getSharedPreferences().getString(valueLabel, "")
     }

}