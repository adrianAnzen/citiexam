package com.nat.examencitianzen.repository

import com.nat.examencitianzen.data.model.Transaction

interface Repository {
    suspend fun getAllTransaction():Transaction
    suspend fun login(user:String,pass:String)
}