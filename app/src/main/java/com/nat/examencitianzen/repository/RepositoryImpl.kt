package com.nat.examencitianzen.repository

import com.nat.examencitianzen.data.local.LocalResourceDataTransaction
import com.nat.examencitianzen.data.model.Transaction

class RepositoryImpl(private val localResourceDataTransaction: LocalResourceDataTransaction):Repository {
    override suspend fun getAllTransaction(): Transaction = localResourceDataTransaction.getAllTransaccion()
    override suspend fun login(user: String, pass: String) = localResourceDataTransaction.login(user,pass)

}