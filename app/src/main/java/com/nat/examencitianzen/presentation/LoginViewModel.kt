package com.nat.examencitianzen.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.nat.examencitianzen.core.Resource
import com.nat.examencitianzen.repository.Repository
import kotlinx.coroutines.Dispatchers

class LoginViewModel(private val repository: Repository):ViewModel() {

    fun fetchLogin(user:String,pass:String) = liveData(Dispatchers.Main) {
        emit(Resource.Loading())

        try {
            emit(Resource.Success(repository.login(user,pass)))
        }
        catch (e:Exception){
            emit(Resource.Failure(e))
        }
    }

    fun fetchTransaction() = liveData(Dispatchers.IO) {
        emit(Resource.Loading())

        try {
            emit(Resource.Success(repository.getAllTransaction()))
        }
        catch (e:Exception){
            emit(Resource.Failure(e))
        }
    }
}

class LoginViewModelFactory(private val repo: Repository): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(Repository::class.java).newInstance(repo)
    }

}