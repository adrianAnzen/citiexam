package com.nat.examencitianzen.data.model

data class Transaction(
    val transactions: List<TransactionX>
)