package com.nat.examencitianzen.data.model

data class TransactionX(
    val amount: String,
    val description: String,
    val id: Int,
    val status: String
)