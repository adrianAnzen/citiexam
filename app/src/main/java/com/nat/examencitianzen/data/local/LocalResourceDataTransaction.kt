package com.nat.examencitianzen.data.local
import com.nat.examencitianzen.data.model.Transaction
import com.nat.examencitianzen.util.ReadJson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

class LocalResourceDataTransaction() {

   suspend fun getAllTransaccion():Transaction{
        withContext(Dispatchers.IO){
            delay(5000)
        }
       return ReadJson.read()
    }

    suspend fun login(user:String,pass:String) {

        withContext(Dispatchers.IO) {
            delay(5000)
            println("Here after a delay of 5 seconds")
        }

    }
}