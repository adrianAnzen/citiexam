package com.nat.examencitianzen.ui

import android.Manifest
import android.annotation.SuppressLint
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.nat.examencitianzen.R
import com.nat.examencitianzen.core.CheckPermission
import com.nat.examencitianzen.util.CheckPermissions
import com.nat.examencitianzen.util.Constants
import com.nat.examencitianzen.util.SharedPreferencesManager

class MainActivity : AppCompatActivity() {
    private var locationManager : LocationManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
        CheckPermission.permission(this)

    }

    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            SharedPreferencesManager.setSomeStringValue("latitud",location.latitude.toString())
            SharedPreferencesManager.setSomeStringValue("longitud",location.longitude.toString())
        }
        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    override fun onStart() {
        super.onStart()
        if (CheckPermissions.checkPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            getposition()
        } else {
           Toast.makeText(this,"Error de permiso GPS",Toast.LENGTH_LONG).show()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (CheckPermissions.isPermissionGranted(permissions, grantResults, *permissions) && permissions.size > 0) {
            if (requestCode == Constants.LOCATION_PERMISSION) {
               getposition()
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun getposition(){

        locationManager?.requestLocationUpdates(
            LocationManager.NETWORK_PROVIDER,
            0L,
            0f,
            locationListener
        )
    }
}