package com.nat.examencitianzen.ui.adapter
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nat.examencitianzen.core.BaseViewHolder
import com.nat.examencitianzen.data.model.Transaction
import com.nat.examencitianzen.data.model.TransactionX
import com.nat.examencitianzen.databinding.DetailTransactionBinding
import com.nat.examencitianzen.databinding.ItemTransactionBinding

class AdapterTransaction(val transaction:Transaction):RecyclerView.Adapter<BaseViewHolder<*>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val itemBinding = DetailTransactionBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        val viewHolder = ViewHolderTransaction(itemBinding)
        return  viewHolder
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
       when(holder){
           is ViewHolderTransaction ->{
              holder.bind(transaction.transactions[position])
           }
       }
    }

    override fun getItemCount(): Int  = transaction.transactions.size

    inner class ViewHolderTransaction(  val binding: DetailTransactionBinding):BaseViewHolder<TransactionX>(binding.root){
        override fun bind(item: TransactionX) {
            binding.editTextTextPassword2.setText(item.id.toString())
            binding.editTextTextPassword3.setText(item.description)
            binding.editTextTextPassword4.setText(item.amount)
            binding.editTextTextPassword5.setText(item.status)

        }

    }
}