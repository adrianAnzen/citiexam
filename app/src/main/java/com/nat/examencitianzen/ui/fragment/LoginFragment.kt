package com.nat.examencitianzen.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.nat.examencitianzen.R
import com.nat.examencitianzen.core.Resource
import com.nat.examencitianzen.data.local.LocalResourceDataTransaction
import com.nat.examencitianzen.databinding.FragmentLoginBinding
import com.nat.examencitianzen.presentation.LoginViewModel
import com.nat.examencitianzen.presentation.LoginViewModelFactory
import com.nat.examencitianzen.repository.RepositoryImpl

class LoginFragment : Fragment(R.layout.fragment_login) {

    private lateinit var binding:FragmentLoginBinding
    private val  viewModel by viewModels<LoginViewModel> {
        LoginViewModelFactory(RepositoryImpl(LocalResourceDataTransaction()))}
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentLoginBinding.bind(view)

        binding.button.setOnClickListener {
            viewModel.fetchLogin(binding.editTextTextPersonName.text.toString(),
                binding.editTextTextPassword.text.toString()
            ).observe(viewLifecycleOwner, Observer {
                when(it){
                    is Resource.Loading ->{
                        binding.progressBar.visibility = View.VISIBLE
                    }

                    is Resource.Success ->{
                        binding.progressBar.visibility = View.GONE

                        findNavController().navigate(R.id.action_loginFragment_to_detailTransacFragment)

                    }

                    is Resource.Failure ->{
                        binding.progressBar.visibility = View.GONE
                    }
                }
            })
        }

    }

}