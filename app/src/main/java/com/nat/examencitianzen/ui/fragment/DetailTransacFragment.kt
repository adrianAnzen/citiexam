package com.nat.examencitianzen.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.nat.examencitianzen.R
import com.nat.examencitianzen.core.Resource
import com.nat.examencitianzen.data.local.LocalResourceDataTransaction
import com.nat.examencitianzen.databinding.FragmentDetailTransacBinding
import com.nat.examencitianzen.presentation.LoginViewModel
import com.nat.examencitianzen.presentation.LoginViewModelFactory
import com.nat.examencitianzen.repository.RepositoryImpl
import com.nat.examencitianzen.ui.adapter.AdapterTransaction
import com.nat.examencitianzen.util.SharedPreferencesManager

class DetailTransacFragment : Fragment(R.layout.fragment_detail_transac) {

    private lateinit var binding: FragmentDetailTransacBinding
    private lateinit var adapterTransaction:AdapterTransaction
    private val  viewModel by viewModels<LoginViewModel> {
        LoginViewModelFactory(RepositoryImpl(LocalResourceDataTransaction()))
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentDetailTransacBinding.bind(view)

        (activity as AppCompatActivity?)!!.supportActionBar?.title ="la ${SharedPreferencesManager.getSomeStringValue("latitud")
        } lon: ${SharedPreferencesManager.getSomeStringValue("longitud")}"

        print("${SharedPreferencesManager.getSomeStringValue("longitud")}")

        viewModel.fetchTransaction().observe(viewLifecycleOwner, Observer {  result->

            when(result){
                is Resource.Loading ->{
                    binding.progressBar2.visibility = View.VISIBLE
                   binding.recyclerView.visibility = View.GONE
                }
                is Resource.Success ->{
                    adapterTransaction = AdapterTransaction(result.data)
                    binding.progressBar2.visibility = View.GONE
                    binding.recyclerView.adapter = adapterTransaction
                    binding.recyclerView.visibility = View.VISIBLE
                }

                is Resource.Failure ->{
                    binding.progressBar2.visibility = View.GONE
                    binding.recyclerView.visibility = View.GONE
                    Toast.makeText(activity?.applicationContext,"Error",Toast.LENGTH_LONG).show()

                }
            }
        })


    }
}