package com.nat.examencitianzen.core

import android.Manifest
import android.app.Activity
import android.content.Context
import com.nat.examencitianzen.MyApp
import com.nat.examencitianzen.util.CheckPermissions
import com.nat.examencitianzen.util.Constants

object CheckPermission {
    fun permission(context: Activity){
        if (CheckPermissions.checkPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // createLocationRequest()
        } else {
            CheckPermissions.requestPermissions(
               context,
                Constants.LOCATION_PERMISSION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }
    }
}